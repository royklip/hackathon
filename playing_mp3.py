# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 09:36:11 2018

@author: TDurieux
"""

import os
from pygame import mixer  # Load the required library


class MusicPlayer(object):
    def __init__(self, music_location=None):
        if music_location is None:
            music_location = os.path.join(os.getcwd(), 'mp3')
        self.music_location = music_location

        self.emotions_track_dict = {
                'neutral': 'Piano Concerto No 21 in C Major.mp3',
                'calm': 'Glow_unregistered.mp3',
                'happy': 'Happy_unregistered.mp3',
                'sad': 'sad violin.mp3',
                'angry': 'The Monster_unregistered.mp3',
                'fearful': 'Radioactive_unregistered.mp3',
                'disgust': 'Pure Disgust_unregistered.mp3',
                'surprised': 'Surprise_unregistered.mp3'
                }
        
    def _combine_emotions_dicts(self, facial_emotion_dict, sound_emotion_dict):        
        highest_value = 0
        highest_emotion = ''

        for key in sound_emotion_dict.keys():
            
            current_value = facial_emotion_dict[key] * sound_emotion_dict[key]
            if current_value > highest_value:
                highest_value = current_value
                highest_emotion = key
                
        return highest_emotion

    def play_song_to_emotion(self, facial_emotion_dict, sound_emotion_dict):
        emotion = self._combine_emotions_dicts(
                facial_emotion_dict, sound_emotion_dict
                )
        track = os.path.join(
                self.music_location, self.emotions_track_dict[emotion]
                )
        # mixer.init()
        # mixer.music.load(track)
        # mixer.music.play()

        return track

music_player = MusicPlayer()

a = {'neutral': 0.1, 'calm': 0.3, 'sad': 0.1}
b = {'neutral': 0.2, 'calm': 0.3, 'sad': 0.1}
assert music_player._combine_emotions_dicts(a,b) == 'calm'

a = {'neutral': 0.1, 'calm': 0.3, 'sad': 0.1}
b = {'neutral': 7, 'calm': 0.3, 'sad': 0.1}

assert music_player._combine_emotions_dicts(a,b) == 'neutral'

a = {'neutral': 0.1, 'calm': 0.3, 'sad': 8}
b = {'neutral': 7, 'calm': 0.3, 'sad': 0.1}

assert music_player._combine_emotions_dicts(a,b) == 'sad'

music_player.play_song_to_emotion(a,b)
