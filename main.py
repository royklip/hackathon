import webcam
from playing_mp3 import MusicPlayer
import video
import text_to_speech
import audio_analysis
from facialrecognition import facial_detection

# set snapshot timer and frame rate for the video
timer = 1
framerate = 3

# welcome message
text_to_speech.tts("Welcome everyone")
text_to_speech.tts("Let's begin. I am looking forward to it!")

# start webcam for snapshots
image_counter = webcam.record(timer)

# make a groupphoto
text_to_speech.tts("This was a good session. Lets take a final photo together")
webcam.group_photo(image_counter)

# Use snapshots for statistics
facial = facial_detection()
properties, faces = facial.collect_results()

if (type(faces) == list()) & (len(faces) > 1):
	emotion_face_1 = properties[faces[0]]['emotions']
	emotion_face_2 = properties[faces[1]]['emotions']
else:
	emotion_face_1 = properties[faces]['emotions']
	emotion_face_2 = ''

emotion_sound = ''

music_player = MusicPlayer()
song = music_player.play_song_to_emotion(emotion_face_1, emotion_face_1)

# convert snapshots to video
video.images_to_video(framerate, song)
text_to_speech.tts("Perfect, thank you for today. I really liked it. I even made a video of our time together.")
video.play_video()
