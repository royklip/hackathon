import cv2
import time

def snapshot(img_counter, frame):
    img_name = "images/opencv_frame_{}.png".format(str(img_counter).zfill(5))
    cv2.imwrite(img_name, frame)
    print("{} written!".format(img_name))
    img_counter += 1

    return img_counter, img_name


def record(timer):
    cam = cv2.VideoCapture(0)
    cv2.namedWindow("AIME")
    img_counter = 0

    while True:
        ret, frame = cam.read()
        cv2.imshow("AIME", frame)
        if not ret:
            break
        k = cv2.waitKey(1)

        if k%256 == 27:
            # ESC pressed
            print("Escape hit, closing...")
            break
        else:
            time.sleep(timer)
            img_counter, img_name = snapshot(img_counter, frame)

    cam.release()
    cv2.destroyAllWindows()

    return img_counter


def group_photo(img_counter):
    cam = cv2.VideoCapture(0)
    cv2.namedWindow("Group photo")
    begin = 0

    while True:
        ret, frame = cam.read()
        cv2.imshow("Group photo", frame)
        if not ret:
            break
        k = cv2.waitKey(1)

        if k % 256 == 32:
            begin = time.time()
        elif (begin != 0) & (time.time() - begin > 3):
            for i in range(15):
                img_counter, img_name = snapshot(img_counter, frame)
                text_on_photo(img_name)
            break

    cam.release()

    cv2.destroyAllWindows()


def text_on_photo(photo):
    from PIL import Image, ImageDraw, ImageFont

    img = Image.open(photo)

    fnt = ImageFont.truetype('/Library/Fonts/arial.ttf', 35)
    d = ImageDraw.Draw(img)
    d.text((230, 10), "Thank you!", font=fnt, fill=(255, 255, 255))

    img.save(photo)
