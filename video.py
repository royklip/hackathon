# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 00:45:49 2018

@author: MRentmeester
"""

import cv2
import os
import moviepy.editor as mpe
import pygame


image_folder = 'images'
video_name = 'video.mp4'
new_video_name = 'video_with_audio.mp4'


def images_to_video(framerate, song):

    images = [img for img in os.listdir(image_folder) if img.endswith(".png")]
    frame = cv2.imread(os.path.join(image_folder, images[0]))
    height, width, layers = frame.shape

    # generate video by concatenating images
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    video = cv2.VideoWriter(video_name, fourcc, framerate, (width, height))

    for image in images:
        video.write(cv2.imread(os.path.join(image_folder, image)))

    cv2.destroyAllWindows()
    video.release()

    # select, snip and add audio
    my_clip = mpe.VideoFileClip(video_name)
    duration_video = my_clip.duration
    audio_background = mpe.AudioFileClip(song)

    final_clip = my_clip.set_audio(audio_background.set_duration(duration_video))
    final_clip.write_videofile(new_video_name)


def play_video():
    pygame.display.set_caption('Aftermovie time!')

    clip = mpe.VideoFileClip('video_with_audio.mp4')
    clip.preview()

    pygame.quit()
