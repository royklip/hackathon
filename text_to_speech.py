import pyttsx3 as pyttsx


def tts(text):
    # set female voice and reduce the speech rate
    engine = pyttsx.init()
    engine.setProperty('voice', "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_EN-US_ZIRA_11.0")
    rate = engine.getProperty('rate')
    engine.setProperty('rate', rate - 50)

    # output speech
    engine.say(text)
    engine.runAndWait()
    engine.stop()
