import json
from os.path import join, dirname
from os import environ
from watson_developer_cloud import VisualRecognitionV3
import pickle
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import  urllib, base64, json
import requests
import glob

class facial_detection:

    def __init__(self):

        # Load variables, key + URL --> Account Carmen
        self.image = 'images/opencv_frame_00006.png'
        self.key_vision = '5a81b22b45f64ff2b870bab5861e7165'
        self.key_face = '3e6486fcd2f343b0afca5b8aad68ec83'

        self.url_vision = "https://westcentralus.api.cognitive.microsoft.com/vision/v1.0/analyze?%s"
        self.url_face = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect?%s"

        self.vision()
        self.facial()

    def vision(self):    

        # Call vision API
        subscription_key = self.key_vision
        headers = {
            # Request headers.
            'Content-Type': 'application/octet-stream',
            'Ocp-Apim-Subscription-Key': subscription_key,
        }

        params = urllib.parse.urlencode({
            #Request parameters. All of them are optional.
            'visualFeatures': 'Categories,Tags,Description,Faces',
            'language': 'en',
            'details': 'Celebrities,Landmarks'
        })

        url = self.url_vision

        with open(self.image, 'rb') as f:
            payload = f.read()

        parsed = json.loads(requests.post(url, headers=headers, data=payload, params=params).content.decode('utf-8'))
        
        self.vision_results = parsed        
        
    def facial(self):

        # Call facial API:
        subscription_key = self.key_face
        
        headers = {
            # Request headers.
            'Content-Type': 'application/octet-stream',
            'Ocp-Apim-Subscription-Key': subscription_key,
        }

        params = {
            'returnFaceId': 'true',
            'returnFaceLandmarks': 'false',
            'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
        }

        url = self.url_face % params

        with open(self.image, 'rb') as f:
            payload = f.read()
            #parsed = json.loads(requests.post(url, headers=headers, data=payload).content)
        parsed = json.loads(requests.request('POST', url, data=payload, headers=headers, params=params).content.decode('utf-8'))
#        json.dumps(parsed, indent=4, sort_keys=True)
 
        self.facial_results = parsed


    def collect_results(self):

        properties = {}
        persons = []

        # Emotions
        emotions_spotify = {'neutral':'neutral', 'contempt':'calm', 'happiness':'happy',
            'sadness':'sad', 'anger':'angry','fear':'fearful',
            'disgust':'disgust', 'surprise':'surprised'}
        
        for i in self.facial_results:
            propertie = {}
            person = i['faceId']
            emotions = i['faceAttributes']['emotion']
            emotions = dict((emotions_spotify[key], value) for (key, value) in emotions.items())

            propertie['emotions'] = emotions

            # Age
            propertie['age'] = i['faceAttributes']['age']
            propertie['gender'] = i['faceAttributes']['gender']
            propertie['glasses'] = i['faceAttributes']['glasses']
            propertie['moustache'] = i['faceAttributes']['facialHair']
            
            properties[person] = propertie
            persons.append(person)

        # Vision --> cellphone in tag == True 

        return properties, person