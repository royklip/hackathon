# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 13:50:18 2018

@author: VBolwerk
"""

import pyaudio
import wave
import time
import pyAudioAnalysis
import os

from pyAudioAnalysis import audioTrainTest as aT

def audio_snapshot(record_seconds, pause_seconds):
  iteration= 0
  while True:
    iteration+=1
    CHUNK = 1024
    FORMAT = pyaudio.paInt16
    CHANNELS = 2
    RATE = 44100
    RECORD_SECONDS = record_seconds
    PAUSE_SECONDS = 7
    WAVE_OUTPUT_FILENAME = "C:/Users/VBolwerk/Documents/Sourcetree/AIME/hackathon/voice{}.wav".format(iteration)
    
    p = pyaudio.PyAudio()
    
    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)
    
    print("* recording")
    
    frames = []
    
    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        frames.append(data)
    
    print("* done recording")
      
    stream.stop_stream()
    stream.close()
    p.terminate()
    
    wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(frames))
    wf.close()
    
    print(aT.fileClassification(inputFile="C:/Users/VBolwerk/Documents/Sourcetree/AIME/hackathon/voice{}.wav".format(iteration), 
                      model_name="C:/Users/VBolwerk/Documents/Sourcetree/AIME/hackathon/svmSMtemp", 
                      model_type="svm"))
    
    arrays = aT.fileClassification(inputFile="C:/Users/VBolwerk/Documents/Sourcetree/AIME/hackathon/voice{}.wav".format(3), 
                      model_name="C:/Users/VBolwerk/Documents/Sourcetree/AIME/hackathon/svmSMtemp", 
                      model_type="svm")
    
    keys = arrays[2]
    values = arrays[1]
    dictionary = dict(zip(keys, values))
    
    print(dictionary)